from rest_framework import status, viewsets
from rest_framework.views import APIView
from user_accounts.serializers import UserSerializer
from user_accounts.models import CustomUser
from rest_framework.response import Response


class RegisterView(APIView):
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class UserListView(APIView):
#     def get()
