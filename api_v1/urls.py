from rest_framework import routers
from .views.announcements import AnnouncementViewSet
from .views.accounts import RegisterView
#from .views.camps import CampViewSet
from django.urls import path
from django.conf.urls import url
from api_v1 import urls as api_v1_urls

urlpatterns = []


router = routers.SimpleRouter()
router.register('announcements', AnnouncementViewSet, 'announcements')
urlpatterns += [path('register/', RegisterView.as_view())]
# urlpatterns += [path('userlist/', UserListView.as_view())]
#router.register('camps', CampViewSet, 'camps')
urlpatterns += router.urls